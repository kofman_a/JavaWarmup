/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passline;

import java.util.Comparator;

/**
 * Game object containing the point, each die, and each player
 * @author 1438408
 */
public class Game {
    private Die die1;
    private Die die2;
    private Player player;
    private int point;
    private int total;
    
    public Game(Die die1, Die die2, Player player) {
        this.die1 = die1;
        this.die2 = die2;
        this.player = player;
        this.point = 0;
        this.total = 0;
    }
    
    /**
     * Sets the total of the game to the total obtained from the dice
     */
    public void setTotal(){
        this.total = this.die1.getNumber() + this.die2.getNumber();
    }
    
    public int getTotal(){
        return this.total;
    }
    
    public void setPoint(int point){
        this.point = point;
    }
    
    public int getPoint(){
        return this.point;
    }
    
    public boolean checkWinTurn(){
        return (this.total == 7 || this.total == 11); 
    }
    
    public boolean checkLoseTurn(){
        return (this.total == 2 || this.total == 3 || this.total == 12);
    }
    
    public boolean checkWinTurnWithPoint(){
        return (this.total == this.point); 
    }
    
    public boolean checkLoseTurnWithPoint(){
        return (this.total == 7);
    }
    
    
    public static boolean tryParseInt(String value) {  
            try {  
                Integer.parseInt(value); 
                return true;  
            } catch (NumberFormatException e) {  
                return false;  
            }  
    }

    
}
