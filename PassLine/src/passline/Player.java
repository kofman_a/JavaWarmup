/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passline;

/**
 * This the class for each player object
 * @author 1438408
 */
public class Player {
    private double bet;
    private double totalMoney;
    private String name;
    
    public Player(){
        this.totalMoney = 0;
        this.bet = 0;
        this.name = "";
    }
    
    public Player(double bet, double totalMoney, String name){
        this.bet = bet;
        this.totalMoney = totalMoney;
        this.name = name;
    }
    
    public void setBet(double bet){
        this.bet = bet;
    }
    
    public double getTotalMoney(){
        return this.totalMoney;
    }
    
    public void winBet(){
        this.totalMoney += bet;
        System.out.println("You won the bet. You currently have $" + this.getTotalMoney() );
    }
    
    public void loseBet(){
        this.totalMoney -= bet;
        System.out.println("You lost the bet. You currently have $" + this.getTotalMoney() );
    }
    
    public boolean checkLoseGame(){
        boolean result = (this.totalMoney <= 0);
        return result;
    }
    
}
