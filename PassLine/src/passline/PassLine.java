

package passline;
import java.util.Scanner;
/**
 *
 * @author 1438408
 */
public class PassLine {

    /**
     * Full game loop
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        String name;
        double bet;
        double total;
        String option;
        boolean winTurnValue;
        boolean loseTurnValue;
        
        //Accepting initial input
        Scanner reader = new Scanner(System.in);
        System.out.println("Hello Player, what is your name?");
        name = reader.nextLine();
        checkEmptyInput(name);
        
        //Accepting total money
        System.out.println("Enter your total amount of money");
        String totalIN = checkEmptyInput(reader.nextLine());
        
        //Ensure it's not a string
        while(!tryParseDouble(totalIN)){
            System.out.println("Please enter a valid amount");
            totalIN = checkEmptyInput(reader.nextLine());
        }
        
        total = Double.parseDouble(totalIN);
        
        System.out.println("Enter your first bet");
        String betIN = checkEmptyInput(reader.nextLine());
        
        while( (!tryParseDouble(betIN)) || Double.parseDouble(betIN) > total ){
            System.out.println("Please enter a valid amount");
            betIN = checkEmptyInput(reader.nextLine());
        }
        
        bet = Double.parseDouble(betIN);
        
        Player p1 = new Player(bet, total,name);
        
        Die d1 = new Die();
        
        Die d2 = new Die();
        
        Game g = new Game(d1,d2,p1);
        
        do{
            System.out.println("Press z to roll");
            System.out.println("Press x to exit");
            
            option = checkOption(reader.nextLine());
            
            switch(option){
                case "x":
                    System.out.println("Thank you for playing. " +name);
                    System.exit(0);
                    break;
                case "z":
                    //If there is no point
                    if(g.getPoint() == 0){
                        makeTurn(d1, d2, g);
                        loseTurnValue = g.checkLoseTurn();
                        winTurnValue = g.checkWinTurn();
                        
                        if(loseTurnValue){
                            p1.loseBet();
                            checkPlayerLost(p1);
                            p1.setBet(reEnterBet(p1.getTotalMoney()));
                        }
                        if(winTurnValue){
                            p1.winBet();
                            p1.setBet(reEnterBet(p1.getTotalMoney()));
                        }
                        //Point is set here
                        if((!winTurnValue) && (!loseTurnValue) ){
                            //Sets point to the total rolled
                            g.setPoint(g.getTotal());
                            System.out.println("The point is now " + g.getPoint());
                        }
                    }
                    //If there is a point
                    else{
                        makeTurn(d1, d2, g);
                        loseTurnValue = g.checkLoseTurnWithPoint();
                        winTurnValue = g.checkWinTurnWithPoint();
                        
                        if(loseTurnValue){
                            p1.loseBet();
                            
                            //Reset the point here
                            resetPoint(g);
                            checkPlayerLost(p1);
                            p1.setBet(reEnterBet(p1.getTotalMoney()));
                        }
                        if(winTurnValue){
                            p1.winBet();
                            resetPoint(g);
                            p1.setBet(reEnterBet(p1.getTotalMoney()));
                        }
                    }
                    break;
                }
            }while(true);

    }
    
    /**
     * @param value the value checking
     */
    public static boolean tryParseInt(String value) {  
	try {  
            Integer.parseInt(value);  
	    return true;  
	    } catch (NumberFormatException e) {  
	    return false;  
	}  
    }
    
    /**
     * @param value checking that user inputs valid option
     */
    public static String checkOption(String opt){
        Scanner retry = new Scanner(System.in);
        
        while(!(opt.equals("z") || opt.equals("x"))){
            System.out.println("Please only input one of the options");
            opt = retry.nextLine();
        }
        return opt;
    }
    /**
     * @param value the value checking
     */
    public static boolean tryParseDouble(String value) {  
	try {  
            Double.parseDouble(value);  
	    return true;  
	    } catch (NumberFormatException e) {  
	    return false;  
	}  
    }
    
    public static String checkEmptyInput(String input){
        Scanner retry = new Scanner(System.in);
        while(input.trim().length() <= 0){
            System.out.println("I won't accept empty input, try again!");
            input = retry.nextLine();
        }
        return input;
    }
    
    public static double reEnterBet(double total){
        String betStr;
        System.out.println("Please enter your bet here");
        Scanner betScan = new Scanner(System.in);
        
        //While the bet is empty, keep asking for input
        betStr = checkEmptyInput(betScan.nextLine());
        
        //While the bet is greater than total or not a double value
        while((!tryParseDouble(betStr)) || Double.parseDouble(betStr) > total ){
            System.out.println("Please enter a proper number");
            betStr = checkEmptyInput(betScan.nextLine());
        }
        
        //return bet string as a double
        return Double.parseDouble(betStr);
    }
    /**
     * Main turn logic
     * @param d1
     * @param d2
     * @param g 
     */
    public static void makeTurn(Die d1, Die d2, Game g ){
          d1.roll();
          d2.roll();
          g.setTotal();
          System.out.println("die 1 is " + d1.getNumber());
          System.out.println("die 2 is " + d2.getNumber());
          System.out.println("You rolled a " + g.getTotal());
    }
    
    /**
     * Check if the player in the params ran out of money
     * @param p1 
     */
    public static void checkPlayerLost(Player p1){
        boolean loseGame = p1.checkLoseGame();
        if(loseGame){
            System.out.println("You lose!You ran out of money.");
            System.exit(1);
        }
    }
    
    public static void resetPoint(Game g){
        g.setPoint(0);
    }
}
