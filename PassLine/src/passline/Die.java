/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package passline;

/**
 *
 * @author 1438408
 */
public class Die {
    private int numRolled;
    public Die() {
        this.numRolled = 0;
    }
    
    public int getNumber(){
        return this.numRolled;
    }
    /**
     * Assigns number to the die
     */
    public void roll(){
        int numRolled = (int)Math.floor(Math.random() * 6) + 1;
        this.numRolled = numRolled;
    }
}
